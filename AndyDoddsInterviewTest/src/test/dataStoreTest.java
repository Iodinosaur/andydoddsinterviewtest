package test;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import data.dataStore;

public class dataStoreTest {

	@Test
	public void testDataStore() {
		dataStore.clearAllData();
		dataStore.addUser("AndyDodds");
		List<String> allNumbers;
		try {
			dataStore.registerNumber("AndyDodds", "00000000000");
			List<String> numbersForUser = dataStore.getAllNumbersForUser("AndyDodds");
			allNumbers = dataStore.getAllNumbers();
			
			Assert.assertEquals(numbersForUser.get(0), allNumbers.get(0));
			
			dataStore.registerNumber("AndyDodds", "11111111111");
			
			numbersForUser = dataStore.getAllNumbersForUser("AndyDodds");
			Assert.assertEquals(numbersForUser.size(), 2);
			
			dataStore.addUser("MikeWazowski");
			dataStore.registerNumber("MikeWazowski", "99999999999");
			
			numbersForUser = dataStore.getAllNumbersForUser("AndyDodds");
			Assert.assertEquals(numbersForUser.size(), 2);
			allNumbers = dataStore.getAllNumbers();
			Assert.assertEquals(allNumbers.size(), 3);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
