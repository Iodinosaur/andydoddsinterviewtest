package bridge;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.List;

import data.dataStore;
import spark.Request;
import spark.Response;
import spark.Route;

public class WebApi {

	public static void main(String[] args) {
		get("/phoneNumbers/getAll", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				return processListOfNumbers(dataStore.getAllNumbers());
			}
		});
		get("/phoneNumbers/getUser", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				String username = request.params("username");
				return processListOfNumbers(dataStore.getAllNumbersForUser(username));
			}
		});
		post("/phoneNumbers/registerNumber", new Route() {

			@Override
			public Object handle(Request request, Response response) throws Exception {
				String username = request.params("username");
				String number = request.params("number");
				
				dataStore.registerNumber(username, number);
				return "Added number to user.";
			}
			
		});
	}
	
	private static String processListOfNumbers(List<String> numbers) {
		String output = "";
		for (String number : numbers) {
			output += number + "\r\n";
		}
		return output;
	}
	
}
