package data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class to manage a telephone number data store
 * @author Andy Dodds
 */
public class dataStore {
	private static Map<String, List<String>> data;
	
	/**
	 * Add a user to the system with no phone number
	 * @param username the users name to register them under
	 */
	public static void addUser(String username) {
		if (data == null) {
			data = new HashMap<String, List<String>>();
		}
		
		data.put(username, new ArrayList<String>());
	}
	
	/**
	 * Registers a phone number to a user
	 * @param username
	 * @param phoneNumber
	 * @throws Exception thrown if the data doesn't exist or a user doesn't exist
	 */
	public static void registerNumber(String username, String phoneNumber) throws Exception{
		if (data == null) {
			throw (new Exception("No users currently stored."));
		}
		List<String> numbers = data.get(username);
		if (numbers == null) {
			throw (new Exception("This user is not yet registered: " + username));
		}
		numbers.add(phoneNumber);	
	}
	
	/**
	 * returns all registered phone numbers. Usernames are not returned.
	 * @return
	 */
	public static List<String> getAllNumbers() throws Exception{
		if (data == null) {
			throw (new Exception("No users currently stored."));
		}
		List<String> combinedData = new ArrayList<String>();
		for (Map.Entry<String, List<String>> dataEntry : data.entrySet()) {
			combinedData.addAll(dataEntry.getValue());
		}
		return combinedData;
	}
	
	public static List<String> getAllNumbersForUser(String username) throws Exception{
		if (data == null) {
			throw (new Exception("No users currently stored."));
		}
		return data.get(username);
	}
	
	public static void clearAllData() {
		data = new HashMap<String, List<String>>();
	}
}
